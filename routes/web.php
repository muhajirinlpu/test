<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('city', function () use ($app) {
	return response()->json(
		[
			"status" => true,
			"data" => \App\City::all()
		]);
});

$app->get('vendor', function () use ($app) {
	return response()->json(
		[
			"status" => true,
			"data" => \App\Vendor::all()
		]);
});

$app->post('vendor/store', 'StoreController@store');
