<?php

namespace App\Http\Controllers;

use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            "vendor_name" => 'required',
            "vendor_adress" => 'required',
            "vendor_city" => 'required',
            "vendor_phone1" => 'required',
            "vendor_phone2" => 'required',
            "vendor_startyear" => 'required',
            "vendor_companyname" => 'required',
            "is_active" => 'required',
            "vendor_username" => 'required',
        ]);

        $vendor = new Vendor();

        $vendor->fill($request->all());
        $vendor->setAttribute('vendor_lastmodified', Carbon::now());

        if ($vendor->save())
            return response()->json(['success' => 1]);

        return response()->json(['success' => 0]);
    }
}
