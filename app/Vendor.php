<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'vendor';

    protected $fillable = [
        "vendor_name",
        "vendor_adress",
        "vendor_city",
        "vendor_postcode",
        "vendor_phone1",
        "vendor_phone2",
        "vendor_fax",
        "vendor_mail",
        "vendor_website",
        "vendor_companytype",
        "vendor_startyear",
        "vendor_PKP",
        "vendor_companyname",
        "is_active",
        "vendor_username",
        "vendor_companylogo",
        "vendor_lastmodified",
        "vendor_id",
    ];

    protected $primaryKey = 'vendor_id';

    public $timestamps = false;
}