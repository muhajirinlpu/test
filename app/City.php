<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    protected $table = 'kota';

    protected $fillable = [
        "kode_kota",
        "NamaKota",
        "Jenis",
        "FK_Propinsi",
    ];
}